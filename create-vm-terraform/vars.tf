variable "PROXMOX_NODE" {
  description = "Node to use for VM creation in proxmox"
  type        = string
}

variable "PROXMOX_API_ENDPOINT" {
  description = "API endpoint for proxmox"
  type        = string
}

variable "PROXMOX_API_TOKEN_ID" {
  description = "API token id to login proxmox"
  type        = string
}

variable "PROXMOX_API_TOKEN_SECRET" {
  description = "API token secret to login proxmox"
  type        = string
  sensitive   = true
}

variable "BRIDGE" {
  description = "Bridge to use when creating VMs in proxmox"
  type        = string
}

variable "CLONE_TEMPLATE" {
  description = "Name of your template"
  type        = string
}

variable "TEMPLATE_USERNAME" {
  description = "Username that's configured in your cloud-init template VM in proxmox"
  type        = string
}

variable "STORAGE" {
  description = "Storage for vm disks"
  type        = string
}

variable "VMID" {
  description = "Start vmid"
  type        = number
}

variable "IP_START" {
  description = "Begin ip network (first tree octet with point, example 192.168.0.)"
  type        = string
}

variable "IP" {
  description = "Start ip last octet"
  type        = number
}

variable "IP_MASK" {
  description = "Network mask, example 24"
  type        = number
}

variable "IP_GATEWAY" {
  description = "ip gateway"
  type        = string
}

variable "SSH_KEY" {
  description = "ssh-rsa key"
  type        = string
  sensitive   = true
}

variable "CLUSTER_VM_PREFIX" {
  description = "prefix for cluster vm name"
  type        = string
  default     = "k8s"
}


variable "k8s_master_config" {
  description = "k8s master config"
  type = object({
    count     = number
    sockets   = number
    cores     = number
    memory    = number
    disk_size = string
    name_format = string
  })
  default = {
    count     = 3
    sockets   = 1
    cores     = 4
    memory    = 4096
    disk_size = "60G"
    name_format = "%scp%s"
  }
}

variable "k8s_ingress_config" {
  description = "k8s ingress config"
  type = object({
    count     = number
    sockets   = number
    cores     = number
    memory    = number
    disk_size = string
    name_format = string
  })
  default = {
    count     = 2
    sockets   = 1
    cores     = 2
    memory    = 2048
    disk_size = "20G"
    name_format = "%sing%s"
  }
}

variable "k8s_worker_config" {
  description = "k8s worker config"
  type = object({
    count     = number
    sockets   = number
    cores     = number
    memory    = number
    disk_size = string
    name_format = string
  })
  default = {
    count     = 3
    sockets   = 1
    cores     = 10
    memory    = 32768
    disk_size = "60G"
    name_format = "%swr%s"
  }
}

variable "common_configs" {
  description = "Common configs between all nodes"
  type = object({
    os_type       = string
    agent         = number
    network_model = string
    cpu           = string
    scsihw        = string
    bootdisk      = string
    disk_type     = string
    iothread      = number
    firewall      = bool
    oncreate      = bool
    numa          = bool
  })
  default = {
    os_type       = "linux"
    agent         = 1
    network_model = "virtio"
    cpu           = "host"
    scsihw        = "virtio-scsi-single"
    bootdisk      = "scsi0"
    disk_type     = "scsi"
    iothread      = 1
    firewall      = false
    oncreate      = false
    numa          = true
  }
}