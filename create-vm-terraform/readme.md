## Создание виртуалок для k8s

Создаем роль

```shell
pveum role add terraform-role -privs "VM.Allocate VM.Clone VM.Config.CDROM VM.Config.CPU VM.Config.Cloudinit VM.Config.Disk VM.Config.HWType VM.Config.Memory VM.Config.Network VM.Config.Options VM.Monitor VM.Audit VM.PowerMgmt Datastore.AllocateSpace Datastore.Audit SDN.Use"
```

Создаем пользователя с этой ролью

```shell
pveum user add terraform@pve
pveum aclmod / -user terraform@pve -role terraform-role
```

Создаем api-token для пользователя

```shell
pveum user token add terraform@pve terraform-token --privsep=0
```

Результат:

```
┌──────────────┬──────────────────────────────────────┐
│ key          │ value                                │
╞══════════════╪══════════════════════════════════════╡
│ full-tokenid │ terraform@pve!terraform-token        │
├──────────────┼──────────────────────────────────────┤
│ info         │ {"privsep":"0"}                      │
├──────────────┼──────────────────────────────────────┤
│ value        │ secret                               │
└──────────────┴──────────────────────────────────────┘
```

Импортируем переменные для terraforma

```shell
source ./.env
```