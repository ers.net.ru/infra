terraform {
  required_providers {
    proxmox = {
      source  = "telmate/proxmox"
      version = "2.9.11"
    }
  }
}

provider "proxmox" {
  pm_api_url          = "https://${var.PROXMOX_API_ENDPOINT}:8006/api2/json"
  pm_api_token_id     = var.PROXMOX_API_TOKEN_ID
  pm_api_token_secret = var.PROXMOX_API_TOKEN_SECRET
  pm_tls_insecure     = true
  pm_parallel         = 4
}

resource "local_file" "ansible_hosts" {

  depends_on = [
    proxmox_vm_qemu.k8s_worker,
    proxmox_vm_qemu.k8s_ingress,
    proxmox_vm_qemu.k8s_master
  ]

  content = templatefile("./file_template/ansible_hosts.yaml.j2",
    {
      node_map_masters = zipmap(
        #        tolist(proxmox_vm_qemu.k8s_master.*.ssh_host), tolist(proxmox_vm_qemu.k8s_master.*.name)
        [for s in proxmox_vm_qemu.k8s_master.*.ipconfig0 : regex("^ip=(.+)/.+$", s)[0]], tolist(proxmox_vm_qemu.k8s_master.*.name)
      ),
      node_map_ingress = zipmap(
        #        tolist(proxmox_vm_qemu.k8s_worker.*.ssh_host), tolist(proxmox_vm_qemu.k8s_worker.*.name)
        [for s in proxmox_vm_qemu.k8s_ingress.*.ipconfig0 : regex("^ip=(.+)/.+$", s)[0]], tolist(proxmox_vm_qemu.k8s_ingress.*.name)
      ),
      node_map_workers = zipmap(
        #        tolist(proxmox_vm_qemu.k8s_worker.*.ssh_host), tolist(proxmox_vm_qemu.k8s_worker.*.name)
        [for s in proxmox_vm_qemu.k8s_worker.*.ipconfig0 : regex("^ip=(.+)/.+$", s)[0]], tolist(proxmox_vm_qemu.k8s_worker.*.name)
      ),
      "ansible_port" = 22,
      "ansible_user" = var.TEMPLATE_USERNAME
    }
  )
  filename = "${path.module}/.output/${var.CLUSTER_VM_PREFIX}_ansible_hosts.yaml"

}

output "ansible_inventory" {
  depends_on = [
    local_file.ansible_hosts
  ]
  value = local_file.ansible_hosts.content
}

resource "local_file" "ssh_config" {

  depends_on = [
    proxmox_vm_qemu.k8s_worker,
    proxmox_vm_qemu.k8s_ingress,
    proxmox_vm_qemu.k8s_master,
    var.CLUSTER_VM_PREFIX
  ]

  content = templatefile("./file_template/ssh_config.j2",
    {
      cluster_name = var.CLUSTER_VM_PREFIX,
      node_map_masters = zipmap(
        [for s in proxmox_vm_qemu.k8s_master.*.ipconfig0 : regex("^ip=(.+)/.+$", s)[0]], tolist(proxmox_vm_qemu.k8s_master.*.name)
      ),
      node_map_ingress = zipmap(
        [for s in proxmox_vm_qemu.k8s_ingress.*.ipconfig0 : regex("^ip=(.+)/.+$", s)[0]], tolist(proxmox_vm_qemu.k8s_ingress.*.name)
      ),
      node_map_workers = zipmap(
        [for s in proxmox_vm_qemu.k8s_worker.*.ipconfig0 : regex("^ip=(.+)/.+$", s)[0]], tolist(proxmox_vm_qemu.k8s_worker.*.name)
      ),
      "ansible_port" = 22,
      "ansible_user" = var.TEMPLATE_USERNAME
    }
  )

  filename = "${path.module}/.output/${var.CLUSTER_VM_PREFIX}_ssh_config"

}

output "ssh_config" {
  depends_on = [
    local_file.ssh_config
  ]
  value = local_file.ssh_config.content
}

# Setting up control plane nodes
resource "proxmox_vm_qemu" "k8s_master" {
  count       = var.k8s_master_config.count
  vmid        = var.VMID + count.index
  name        = format(var.k8s_master_config.name_format, var.CLUSTER_VM_PREFIX, count.index)
  desc        = "Master node in k8s cluster"
  os_type     = var.common_configs.os_type
  clone       = var.CLONE_TEMPLATE
  full_clone  = true
  agent       = var.common_configs.agent
  oncreate    = var.common_configs.oncreate
  numa        = var.common_configs.numa
  target_node = var.PROXMOX_NODE

  memory  = var.k8s_master_config.memory
  sockets = var.k8s_master_config.sockets
  cores   = var.k8s_master_config.cores

  scsihw   = var.common_configs.scsihw
  bootdisk = var.common_configs.bootdisk

  disk {
    slot = 0
    # set disk size here. leave it small for testing because expanding the disk takes time.
    size     = var.k8s_master_config.disk_size
    type     = var.common_configs.disk_type
    storage  = var.STORAGE
    iothread = var.common_configs.iothread
  }

  network {
    model  = var.common_configs.network_model
    bridge = var.BRIDGE
  }

  lifecycle {
    ignore_changes = [
      network,
    ]
  }

  # the ${count.index + 1} thing appends text to the end of the ip address
  # in this case, since we are only adding a single VM, the IP will
  # be 10.98.1.91 since count.index starts at 0. this is how you can create
  # multiple VMs and have an IP assigned to each (.91, .92, .93, etc.)
  ipconfig0 = "ip=${var.IP_START}${var.IP
                                    + count.index}/${var.IP_MASK},gw=${var.IP_GATEWAY}"
  #  ssh_forward_ip = "192.168.1.${var.IP + count.index}"

  # sshkeys set using variables. the variable contains the text of the key.
  sshkeys = <<EOF
  ${var.SSH_KEY}
  EOF

}

# Setting up ingress nodes
resource "proxmox_vm_qemu" "k8s_ingress" {
  count       = var.k8s_ingress_config.count
  vmid        = var.VMID + var.k8s_master_config.count + count.index
  name        = format(var.k8s_ingress_config.name_format, var.CLUSTER_VM_PREFIX, count.index)
  desc        = "Ingress node in k8s cluster"
  os_type     = var.common_configs.os_type
  clone       = var.CLONE_TEMPLATE
  full_clone  = true
  agent       = var.common_configs.agent
  oncreate    = var.common_configs.oncreate
  numa        = var.common_configs.numa
  target_node = var.PROXMOX_NODE

  memory  = var.k8s_ingress_config.memory
  sockets = var.k8s_ingress_config.sockets
  cores   = var.k8s_ingress_config.cores

  scsihw   = var.common_configs.scsihw
  bootdisk = var.common_configs.bootdisk
  disk {
    slot = 0
    # set disk size here. leave it small for testing because expanding the disk takes time.
    size     = var.k8s_ingress_config.disk_size
    type     = var.common_configs.disk_type
    storage  = var.STORAGE
    iothread = var.common_configs.iothread
  }

  network {
    model  = var.common_configs.network_model
    bridge = var.BRIDGE
  }

  lifecycle {
    ignore_changes = [
      network,
    ]
  }

  ipconfig0 = "ip=${var.IP_START}${var.IP
                                    + var.k8s_master_config.count
                                    + count.index}/${var.IP_MASK},gw=${var.IP_GATEWAY}"
  #  ssh_forward_ip = "192.168.1.${var.IP + var.k8s_master_config.count + count.index}"

  # sshkeys set using variables. the variable contains the text of the key.
  sshkeys = <<EOF
  ${var.SSH_KEY}
  EOF

}

# Setting up worker nodes
resource "proxmox_vm_qemu" "k8s_worker" {
  count       = var.k8s_worker_config.count
  vmid        = var.VMID + var.k8s_master_config.count + var.k8s_ingress_config.count + count.index
  name        = format(var.k8s_worker_config.name_format, var.CLUSTER_VM_PREFIX, count.index)
  desc        = "Worker node in k8s cluster"
  os_type     = var.common_configs.os_type
  clone       = var.CLONE_TEMPLATE
  full_clone  = true
  agent       = var.common_configs.agent
  oncreate    = var.common_configs.oncreate
  numa        = var.common_configs.numa
  target_node = var.PROXMOX_NODE
  memory      = var.k8s_worker_config.memory
  sockets     = var.k8s_worker_config.sockets
  cores       = var.k8s_worker_config.cores

  scsihw   = var.common_configs.scsihw
  bootdisk = var.common_configs.bootdisk
  disk {
    slot = 0
    # set disk size here. leave it small for testing because expanding the disk takes time.
    size     = var.k8s_worker_config.disk_size
    type     = var.common_configs.disk_type
    storage  = var.STORAGE
    iothread = 1
  }

  network {
    model    = var.common_configs.network_model
    bridge   = var.BRIDGE
    firewall = var.common_configs.firewall
  }

  lifecycle {
    ignore_changes = [
      network,
    ]
  }

  ipconfig0 = "ip=${var.IP_START}${var.IP
                                    + var.k8s_master_config.count
                                    + var.k8s_ingress_config.count
                                    + count.index}/${var.IP_MASK},gw=${var.IP_GATEWAY}"

  # sshkeys set using variables. the variable contains the text of the key.
  sshkeys = <<EOF
  ${var.SSH_KEY}
  EOF

}
