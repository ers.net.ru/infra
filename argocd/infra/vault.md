Инициализация

```shell
kubectl exec -ti vault-server-0 -- vault operator init
```

Unseal `vault-server-0`

```shell
kubectl exec -ti vault-server-0 -- vault operator unseal
```

Join `vault-server-1`

```shell
kubectl exec -ti vault-server-1 -- vault operator raft join http://vault-server-active:8200
```

Unseal `vault-server-1`

```shell
kubectl exec -ti vault-server-1 -- vault operator unseal
```

Join `vault-server-2`

```shell
kubectl exec -ti vault-server-2 -- vault operator raft join http://vault-server-active:8200
```

Unseal `vault-server-2`

```shell
kubectl exec -ti vault-server-2 -- vault operator unseal
```

### Настройка для External Secrets Operator


Пробрасываем на локаль

```shell
kubectl port-forward svc/vault-server-active 8200:8200 -n vault
```

Export vault address

```shell
export VAULT_ADDR=http://127.0.0.1:8200
```

Login with root token

```shell
vault login
```

Enable new kv engine

```
vault secrets enable -version=2 -path=argocd kv
```

To list all your current secerets

```
vault secrets list
```

Create test secret

```
vault kv put argocd/mysecret foo=bar
```

Enable approle at path approle

```
vault auth enable approle
```

Create a vault policy

```
vault policy write argocd-read-policy -<<EOF
# Read-only permission on secrets stored at 'argocd/data'
path "argocd/*" {
  capabilities = [ "read", "list" ]
}
EOF
```

Now create a role

```
vault write auth/approle/role/argocd token_policies="argocd-read-policy"
```

Read Policy

```
vault read auth/approle/role/argocd
```

Get role-id

```shell
vault read auth/approle/role/argocd/role-id
```

```
Key     Value
---     -----
role_id 675a50e7-cfe0-be76-e35f-49ec009731ea
```

Get Secret ID

```shell
vault write -force auth/approle/role/argocd/secret-id
```

```
Key                 Value
---                 -----
secret_id           ed0a642f-2acf-c2da-232f-1b21300d5f29
secret_id_accessor  a240a31f-270a-4765-64bd-94ba1f65703c
```

Login using your app role

```shell
vault write auth/approle/login \
role_id="675a50e7-cfe0-be76-e35f-49ec009731ea" \
secret_id="ed0a642f-2acf-c2da-232f-1b21300d5f29"
```

List secret

```shell
vault kv list argocd/
```

Read Secert

```shell
vault kv get test3/mysecret
```


Create secret with secret_id `argocd-approle-secret.yaml`

Create cluster store secrets `cluster-secret-store.yaml`

Example external secret

```yaml
apiVersion: external-secrets.io/v1beta1
kind: ExternalSecret
metadata:
  name: vault-external-secret
  namespace: default
spec:
  refreshInterval: "15s"
  secretStoreRef:
    name: vault-css
    kind: ClusterSecretStore
  target:
    name: example-secret # Kubernetes secret name
  data:
    - secretKey: foobar # This will be the key in kubernetes secret.
      remoteRef: # Vault information about secret
        key: argocd/data/mysecret # Path of secret
        property: foo # Key of secret in vault
```


